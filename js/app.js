// Application 
(function(){

	//Define submodules with no dependencies
	angular.module('app.catalog', []);

	// Define main module, submodules and dependencies
	angular.module('app', [
        'app.catalog', 
        'ngSanitize', 
        'MassAutoComplete' 
    ]);

})();

// Catalog module
(function(){

	angular
		.module('app.catalog')
		.controller('CatalogController', CatalogController)
        .factory('CatalogService', CatalogService)

        // Dependency injection
		CatalogController.$inject = ['CatalogService', '$sce', '$q'];
		CatalogService.$inject = ['$http'];


        // Controller
		function CatalogController(CatalogService, $sce, $q) {
            var vm = this;

            vm.list = [];
            vm.selected = {};
            vm.showDetails = false;
            vm.website = "#";

            vm.autocomplete_options = {
                suggest: searchCatalog,
                on_select: searchMovie
            };

            vm.onInputChange = onInputChange;

            function onInputChange() {
                if (vm.searchTerm.value == "") {
                    vm.showDetails = false;
                }
            }

            function searchCatalog(term) {
                var deffered = $q.defer();
                deffered.resolve(CatalogService.search(term).then(function(response) {
                    var q = term.toLowerCase().trim();
                    var results = [];
                    for (var i = 0; i < response.data.Search.length && results.length < 3; i++) {
                        var state = response.data.Search[i].Title;
                        if (state.toLowerCase().indexOf(q) === 0) {
                            results.push({ label: state, value: state });
                        }
                    }

                    return results;

                }));

                return deffered.promise;
            }

            function searchMovie(selected) {
                CatalogService.searchMovie(selected.value).then(function(response) {
                    vm.selected = response.data;
                    vm.showDetails = true;

                    if ((vm.selected.Website != undefined) &&(vm.selected.Website != "N/A")) {
                        vm.website = vm.selected.Website;
                        if (document.getElementById("websitelink") != null) {
                            document.getElementById("websitelink").target = "_blank";
                        }
                        
                    } else {
                        vm.website = "#";
                         if (document.getElementById("websitelink") != null) {
                            document.getElementById("websitelink").target = "_self";
                         }
                    }
                });
            }

            return vm;

        }


        // Service
		function CatalogService($http) {
            var service = {
				search: search,
                searchMovie: searchMovie
			};


			return service;

			function search(term) {
				return $http.get('http://www.omdbapi.com/?s=' + term);
			}

            function searchMovie(title) {
				return $http.get('http://www.omdbapi.com/?t=' + title);
			}
        }

})();